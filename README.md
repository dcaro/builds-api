# Toolforge builds API

API to start builds of images to use in toolforge.

You can find more documentation about the Toolforge Build Service here:
https://wikitech.wikimedia.org/wiki/Portal:Toolforge/Admin/Build_Service

# Developing

## If you modify the openapi definition

The core of the app is generated by oapi-codegen, from the openapi yaml file, so
every time you modify the openapi yaml definiton, you have to regenerate the
code, to do so just:

```
> make gen-api
```

Then you can manually modify the code under `cmd/main.go` to match the new
changes if necessary.

# Building the docker image

You can just:

```
> make image
```

That will create a `toolsbeta-harbor.wmcloud.org/toolforge/builds-api:dev` image
locally, that you can then re-tag and push if you want.

# Updating the code after changing the openapi yaml file

## On the api side

```
> make gen-api
```

Then you might have to tweak `cmd/main.go` to add any new configuration, or
`internal/api.go` to add or remove old handlers or change any if needed.

# Testing locally

## Static tests

```
> pre-commit run -a
```

## Unit tests

```
> go test ./...
```

## Deploy locally

Prerequisites:

- Minikube
- Toolforge
  [Cert-manager](https://gitlab.wikimedia.org/repos/cloud/toolforge/cert-manager)
- Toolforge
  [api-gateway](https://gitlab.wikimedia.org/repos/cloud/toolforge/api-gateway)
- Toolforge [buildservice](https://github.com/toolforge/buildservice)

## Running outside k8s

You will still need some k8s cluster for it to work as it will create the builds
there, but you can run the server outside of it and it will use your
`$KUBECONFIG` kubernetes configuration.

For that you can:

```
> OUT_OF_K8S_RUN=true go run cmd/main.go
```

## Inside k8s

It should work with both podman/docker and kind/minikube out of the box:

```
> make build-and-deploy-local
```

### To only build the image

You can run:

```
> make image
```

### To only rollout a built image

```
> make rollout
```

### TO only deploy the chart changes

```
> ./deploy.sh
```

## Access locally

With this, you can start a tunnel to expose the api gateway with:

```
> minikube service api-gateway -n api-gateway
```

And do a curl request:

```
> curl --cert ~/.minikube/profiles/minikube/client.crt --key ~/.minikube/profiles/minikube/client.key --insecure 'https://192.168.49.2:30003/builds/v1/healthz'
```

Note that you will have to run a build to see it's logs :)
